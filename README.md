# git-quick-stats

```yaml
stages:
  - 🔎code-quality

include:
  - project: 'make-ci-lovable/notes-api/git-quick-stats'
    file: 'git-quick-stats.gitlab-ci.yml'

🤖:git-analysis:
  stage: 🔎code-quality
  extends: .git-quick-stats:analyzer
  rules:
    #- if: $CI_COMMIT_BRANCH == "master"
    - if: $CI_MERGE_REQUEST_IID

```
