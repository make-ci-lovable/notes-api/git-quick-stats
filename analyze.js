#!/usr/bin/env node

const fs = require("fs")

try {
  // ================================
  //  Generate the Git Sats report
  // ================================
  let report = fs.readFileSync("./detailed-git-stats.txt", "utf8")

  let lines = report.split("\n\t").map(item => item.trim())
  let totalPosition = lines.indexOf("total:")
  let jsonStats = lines.slice(totalPosition+1).map(item => {
    let label = item.split(":")[0]
    let value = item.split("\t")[0].split(":")[1].trim()
    return {label, value}
  })

  let statsReport = jsonStats.map(item => {
    let row = ""
    if(item.label=="insertions") row = `| 🟢 | ${item.label} | ${item.value} |`
    if(item.label=="deletions")  row = `| 🔴 | ${item.label} | ${item.value} |` 
    if(item.label=="files")      row = `| 📝 | ${item.label} | ${item.value} |` 
    if(item.label=="commits")    row = `| ✅ | ${item.label} | ${item.value} |` 
    return row
  })

  // ================================
  //  Generate the reviewers report
  // ================================
  let reviewers = fs.readFileSync("./suggest-reviewers.txt", "utf8")
  
  let reviewersReport = reviewers.split("\n")
    .map(item => item.trim())
    .slice(2).filter(item => item != "")
    .map((item, index) => `| 👀 | reviewer ${index+1} | ${item.split(" ").slice(1).join(" ")} |`)

  let gitReport = [
    "|        | labels | metrics |", 
    "| ------ | ------ | ------  |"
    ]
    .concat(statsReport)
    .concat(reviewersReport)

  try {
    fs.writeFileSync("./git-report.md", gitReport.join("\n"))
  } catch(error) {
    fs.writeFileSync("./git-report.md", "[😡 ${error}]")
  }

} catch(error) {
  fs.writeFileSync("./git-report.md", "[😡 ${error}]")
}


