# git-quick-stats helpers 2020-10-28 by @k33g | on gitlab.com 
FROM arzzen/git-quick-stats

LABEL maintainer="@k33g_org"
LABEL authors="@k33g_org"
LABEL version="1.0"

RUN apk --update add --no-cache nodejs curl

COPY analyze.js /usr/local/bin/analyze
RUN chmod +x /usr/local/bin/analyze

CMD ["/bin/sh"]

